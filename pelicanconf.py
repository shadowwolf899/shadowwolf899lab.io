#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Jericho Keyne'
SITENAME = u'Test Blog'
SITEURL = 'https://shadowwolf899.gitlab.io'

PATH = 'content'
OUTPUT_PATH = 'public'

STATIC_PATHS = ['videos']

TIMEZONE = 'America/New_York'

DEFAULT_LANG = u'enUS'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('GitLab', 'https://gitlab.com'),
         ('Reddit', 'http://reddit.com'),)

# Social widget
SOCIAL = (('Email', 'mailto:jerichokeyne@gmail.com'),
          ('Reddit', 'http://reddit.com/u/shadowwolf899'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'bootstrap'
